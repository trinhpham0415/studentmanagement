insert into student(age, name, created_on, modified_on) values(26, 'Trinh Pham', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into student(age, name, created_on, modified_on) values(28, 'Mai Nguyen', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());
insert into student(age, name, created_on, modified_on) values(24, 'Ha Pham', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP());

insert into address(address, created_on, modified_on, student_id) values ('16 Nguyen Van Thanh', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
insert into address(address, created_on, modified_on, student_id) values ('24 Nguyen Mai Han', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 1);
insert into address(address, created_on, modified_on, student_id) values ('28 Nguyen Mai Hung', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 2);
insert into address(address, created_on, modified_on, student_id) values ('29 Nguyen Ngoc Han', CURRENT_TIMESTAMP(), CURRENT_TIMESTAMP(), 2);