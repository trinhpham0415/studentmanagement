package com.example.demo.exception;

import com.example.demo.constant.Constants;
import com.example.demo.domain.response.Response;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionsHandler {

  @ExceptionHandler(ResourceNotFoundException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public Response handleResourceNotFoundException(ResourceNotFoundException exception) {
    return new Response(exception.getMessage());
  }

  @ExceptionHandler(BadRequestException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Response handleBadRequestException(BadRequestException exception) {
    return new Response(exception.getMessage());
  }

  @ExceptionHandler(ConstraintViolationException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public Response handleConstraintViolationException(ConstraintViolationException exception) {
    Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
    if (violations.size() > 1) {
      StringBuilder messages = new StringBuilder();
      violations.forEach(e -> messages.append(e.getMessage()).append(Constants.SEMICOLON));
      return new Response(messages.toString());
    }
    String message = exception.getMessage();
    return new Response(message.substring(message.indexOf(Constants.COLON) + 2));
  }

}
