package com.example.demo.domain.request;

import com.example.demo.entity.Address;
import java.util.List;

public class NewStudent {

  private String name;
  private int age;
  private List<Address> addresses;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

  public NewStudent() {}

  @Override
  public String toString() {
    return "NewStudent{" +
        "name='" + name + '\'' +
        ", age=" + age +
        ", addresses=" + addresses +
        '}';
  }

}
