package com.example.demo.domain.request;

public class NewAddress {

  private String address;

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public NewAddress() {}

  @Override
  public String toString() {
    return "NewAddress{" +
        "value='" + address + '\'' +
        '}';
  }

}
