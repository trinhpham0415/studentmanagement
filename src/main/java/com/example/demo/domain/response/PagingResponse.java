package com.example.demo.domain.response;

public class PagingResponse {

  private long totalElements;
  private int totalPages;
  private Object data;

  public long getTotalElements() {
    return totalElements;
  }

  public void setTotalElements(long totalElements) {
    this.totalElements = totalElements;
  }

  public int getTotalPages() {
    return totalPages;
  }

  public void setTotalPages(int totalPages) {
    this.totalPages = totalPages;
  }

  public Object getData() {
    return data;
  }

  public void setData(Object data) {
    this.data = data;
  }

  public PagingResponse() {}

  public PagingResponse(long totalElements, int totalPages, Object data) {
    this.totalElements = totalElements;
    this.totalPages = totalPages;
    this.data = data;
  }

  @Override
  public String toString() {
    return "PagingResponse{" +
        "totalElements=" + totalElements +
        ", totalPages=" + totalPages +
        ", data=" + data +
        '}';
  }

}
