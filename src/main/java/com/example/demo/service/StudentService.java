package com.example.demo.service;

import com.example.demo.domain.request.NewStudent;
import com.example.demo.domain.request.UpdatingStudent;
import com.example.demo.domain.response.Response;

public interface StudentService {

  Response createStudent(NewStudent newStudent);
  Response getAllStudents(int page, int pageSize);
  Response getStudentById(Integer studentId);
  Response updateNameOrAge(Integer studentId, UpdatingStudent student);
  Response deleteStudent(Integer studentId);

}
