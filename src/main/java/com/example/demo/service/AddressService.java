package com.example.demo.service;

import com.example.demo.domain.request.NewAddress;
import com.example.demo.domain.response.Response;

public interface AddressService {

  Response createAddress(Integer studentId, NewAddress newAddress);
  Response getAllAddresses(int page, int pageSize);
  Response getAddressById(Integer addressId);
  Response updateAddress(Integer addressId, NewAddress address);
  Response deleteAddress(Integer addressId);

}
