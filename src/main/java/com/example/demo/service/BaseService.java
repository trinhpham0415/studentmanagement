package com.example.demo.service;

import com.example.demo.constant.Constants;
import java.util.Locale;
import javax.annotation.Resource;
import org.springframework.context.MessageSource;
import org.springframework.lang.Nullable;

public class BaseService {

  @Resource
  private MessageSource messageSource;

  public String getMessage(String code) {
    return messageSource.getMessage(code, null, Constants.EMPTY_STRING, Locale.US);
  }

  public String getMessage(String code, @Nullable Object[] args) {
    return messageSource.getMessage(code, args, Constants.EMPTY_STRING, Locale.US);
  }

}
