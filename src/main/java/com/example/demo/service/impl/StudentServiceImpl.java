package com.example.demo.service.impl;

import com.example.demo.constant.Messages;
import com.example.demo.domain.request.NewStudent;
import com.example.demo.domain.request.UpdatingStudent;
import com.example.demo.domain.response.PagingResponse;
import com.example.demo.domain.response.Response;
import com.example.demo.entity.Student;
import com.example.demo.exception.BadRequestException;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.BaseService;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StudentServiceImpl extends BaseService implements StudentService {

  private final StudentRepository studentRepository;
  private final AddressRepository addressRepository;

  @Autowired
  public StudentServiceImpl(StudentRepository studentRepository,
                            AddressRepository addressRepository) {
    this.studentRepository = studentRepository;
    this.addressRepository = addressRepository;
  }

  @Transactional(rollbackFor = {Exception.class})
  @Override
  public Response createStudent(NewStudent newStudent) {
    Student student = new Student(newStudent.getName(), newStudent.getAge(), newStudent.getAddresses());

    //map ManyToOne relationship between address and student
    student.getAddresses().forEach(address -> {
      address.setStudent(student);
    });

    return new Response(HttpStatus.CREATED.getReasonPhrase(), studentRepository.save(student));
  }

  @Override
  public Response getAllStudents(int page, int pageSize) {
    Page<Student> studentPage = studentRepository.findAll(PageRequest.of(page - 1, pageSize));
    return new Response(HttpStatus.OK.getReasonPhrase(),
      new PagingResponse(studentPage.getTotalElements(), studentPage.getTotalPages(),
          studentPage.getContent()));
  }

  @Override
  public Response getStudentById(Integer studentId) {
    return studentRepository.findById(studentId)
        .map(student -> new Response(HttpStatus.OK.getReasonPhrase(), student))
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.STUDENT_NOT_FOUND, new Object[] {studentId})));
  }

  @Override
  public Response updateNameOrAge(Integer studentId, UpdatingStudent student) {
    if (studentId != null && !studentId.equals(student.getId())) {
      throw new BadRequestException(
          getMessage(Messages.STUDENT_ID_NOT_EQUAL, new Object[] {studentId, student.getId()}));
    }

    return studentRepository.findById(studentId)
        .map(currentStudent -> {
          currentStudent.setAge(student.getAge());
          currentStudent.setName(student.getName());
          return new Response(HttpStatus.OK.getReasonPhrase(), studentRepository.save(currentStudent));
        })
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.STUDENT_NOT_FOUND, new Object[] {studentId})));
  }

  @Override
  public Response deleteStudent(Integer studentId) {
    return studentRepository.findById(studentId)
      .map(student -> {
        studentRepository.deleteById(studentId);
        return new Response(HttpStatus.OK.getReasonPhrase());
      })
      .orElseThrow(() -> new ResourceNotFoundException(
          getMessage(Messages.STUDENT_NOT_FOUND, new Object[] {studentId})));
  }
}
