package com.example.demo.service.impl;

import com.example.demo.constant.Messages;
import com.example.demo.domain.request.NewAddress;
import com.example.demo.domain.response.PagingResponse;
import com.example.demo.domain.response.Response;
import com.example.demo.entity.Address;
import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.repository.AddressRepository;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.AddressService;
import com.example.demo.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl extends BaseService implements AddressService {

  private final AddressRepository addressRepository;
  private final StudentRepository studentRepository;

  @Autowired
  public AddressServiceImpl(AddressRepository addressRepository,
                            StudentRepository studentRepository) {
    this.addressRepository = addressRepository;
    this.studentRepository = studentRepository;
  }

  @Override
  public Response createAddress(Integer studentId, NewAddress newAddress) {
    Address address = studentRepository.findById(studentId)
        .map(student -> addressRepository.save(new Address(newAddress.getAddress(), student)))
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.STUDENT_NOT_FOUND, new Object[] {studentId})));

    return new Response(HttpStatus.CREATED.getReasonPhrase(), address);
  }

  @Override
  public Response getAllAddresses(int page, int pageSize) {
    Page<Address> addressPage = addressRepository.findAll(PageRequest.of(page - 1, pageSize));
    return new Response(HttpStatus.OK.getReasonPhrase(),
        new PagingResponse(addressPage.getTotalElements(), addressPage.getTotalPages(),
            addressPage.getContent()));
  }

  @Override
  public Response getAddressById(Integer addressId) {
    return addressRepository.findById(addressId)
        .map(address -> new Response(HttpStatus.OK.getReasonPhrase(), address))
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.ADDRESS_NOT_FOUND, new Object[] {addressId})));
  }

  @Override
  public Response updateAddress(Integer addressId, NewAddress updatingAddress) {
    return addressRepository.findById(addressId)
        .map(address -> {
          address.setAddress(updatingAddress.getAddress());
          return new Response(HttpStatus.OK.getReasonPhrase(), addressRepository.save(address));
        })
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.ADDRESS_NOT_FOUND, new Object[] {addressId})));
  }

  @Override
  public Response deleteAddress(Integer addressId) {
    return addressRepository.findById(addressId)
        .map(address -> {
          addressRepository.deleteById(addressId);
          return new Response(HttpStatus.OK.getReasonPhrase());
        })
        .orElseThrow(() -> new ResourceNotFoundException(
            getMessage(Messages.ADDRESS_NOT_FOUND, new Object[] {addressId})));
  }
}
