package com.example.demo.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public class BaseEntity {

  @Column(name = "created_on")
  @CreationTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  private Date createdOn;
  @Column(name = "modified_on")
  @UpdateTimestamp
  @Temporal(TemporalType.TIMESTAMP)
  private Date modifiedOn;

  public Date getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Date createdOn) {
    this.createdOn = createdOn;
  }

  public Date getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(Date modifiedOn) {
    this.modifiedOn = modifiedOn;
  }

  public BaseEntity() {}

  @Override
  public String toString() {
    return "BaseEntity{" +
        "createdOn=" + createdOn +
        ", modifiedOn=" + modifiedOn +
        '}';
  }
}
