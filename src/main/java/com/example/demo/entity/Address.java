package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "address")
public class Address extends BaseEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "address_id")
  private Integer id;

  @Column(name = "address")
  private String address;

  @ManyToOne
  @JoinColumn(name = "student_id")
  @JsonIgnoreProperties("addresses")
  private Student student;

  public Address() {}

  public Address(String address) {
    this.address = address;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public Student getStudent() {
    return student;
  }

  public void setStudent(Student student) {
    this.student = student;
  }

  public Address(String address, Student student) {
    this.address = address;
    this.student = student;
  }

  @Override
  public String toString() {
    return "Address{" +
        "id=" + id +
        ", value='" + address + '\'' +
        ", student=" + student +
        '}';
  }
}
