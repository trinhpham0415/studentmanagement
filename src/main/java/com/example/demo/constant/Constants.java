package com.example.demo.constant;

public class Constants {

  public static final String EMPTY_STRING = "";
  public static final String SEMICOLON = "; ";
  public static final String COLON = ":";

}
