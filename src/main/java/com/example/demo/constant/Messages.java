package com.example.demo.constant;

public class Messages {

  public static final String STUDENT_NOT_FOUND = "student.error.notFound";
  public static final String STUDENT_ID_NOT_EQUAL = "student.error.studentIdNotEqual";
  public static final String ADDRESS_NOT_FOUND = "address.error.notFound";
  public static final String PAGE_ERROR_MIN = "page must not be less than {value}";
  public static final String PAGE_SIZE_ERROR_MIN = "pageSize must not be less than {value}";

}
