package com.example.demo.constant;

public class Endpoints {

  public static final String URI_ID = "/{id}";
  public static final String ID = "id";
  public static final String URI_STUDENTS = "/students";
  public static final String URI_ADDRESSES = "/addresses";
  public static final String URI_PAGE = "page";
  public static final String URI_PAGE_SIZE = "pageSize";

}
