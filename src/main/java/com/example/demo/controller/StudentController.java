package com.example.demo.controller;

import com.example.demo.constant.Endpoints;
import com.example.demo.constant.Messages;
import com.example.demo.domain.request.NewAddress;
import com.example.demo.domain.request.NewStudent;
import com.example.demo.domain.request.UpdatingStudent;
import com.example.demo.domain.response.Response;
import com.example.demo.service.AddressService;
import com.example.demo.service.StudentService;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.URI_STUDENTS)
@Validated
public class StudentController {

  private final StudentService studentService;
  private final AddressService addressService;

  @Autowired
  public StudentController(StudentService studentService, AddressService addressService) {
    this.studentService = studentService;
    this.addressService = addressService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Response createStudent(@RequestBody NewStudent newStudent) {
    return studentService.createStudent(newStudent);
  }


  @PostMapping(Endpoints.URI_ID + Endpoints.URI_ADDRESSES)
  @ResponseStatus(HttpStatus.CREATED)
  public Response addAddressForStudent(@PathVariable(Endpoints.ID) Integer studentId,
                                @RequestBody NewAddress newAddress) {
    return addressService.createAddress(studentId, newAddress);
  }

  @GetMapping
  public Response getAllStudents(
      @RequestParam(Endpoints.URI_PAGE) @Valid @Min(value = 1, message = Messages.PAGE_ERROR_MIN) int page,
      @RequestParam(Endpoints.URI_PAGE_SIZE) @Valid @Min(value = 1, message = Messages.PAGE_SIZE_ERROR_MIN) int pageSize) {
    return studentService.getAllStudents(page, pageSize);
  }

  @GetMapping(Endpoints.URI_ID)
  public Response getStudentById(@PathVariable(Endpoints.ID) Integer studentId) {
    return studentService.getStudentById(studentId);
  }

  @PatchMapping(Endpoints.URI_ID)
  public Response updateNameOrAge(@PathVariable(Endpoints.ID) Integer studentId,
                                @RequestBody UpdatingStudent student) {
    return studentService.updateNameOrAge(studentId, student);
  }

  @DeleteMapping(Endpoints.URI_ID)
  public Response deleteStudent(@PathVariable(Endpoints.ID) Integer studentId) {
    return studentService.deleteStudent(studentId);
  }

}
