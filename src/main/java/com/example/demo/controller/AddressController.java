package com.example.demo.controller;

import com.example.demo.constant.Endpoints;
import com.example.demo.constant.Messages;
import com.example.demo.domain.request.NewAddress;
import com.example.demo.domain.response.Response;
import com.example.demo.service.AddressService;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Endpoints.URI_ADDRESSES)
@Validated
public class AddressController {

  private final AddressService addressService;

  @Autowired
  public AddressController(AddressService addressService) {
    this.addressService = addressService;
  }

  @GetMapping
  public Response getAllAddresses(
      @RequestParam(Endpoints.URI_PAGE) @Valid @Min(value = 1, message = Messages.PAGE_ERROR_MIN) int page,
      @RequestParam(Endpoints.URI_PAGE_SIZE) @Valid @Min(value = 1, message = Messages.PAGE_SIZE_ERROR_MIN) int pageSize) {
    return addressService.getAllAddresses(page, pageSize);
  }

  @GetMapping(Endpoints.URI_ID)
  public Response getAddressById(@PathVariable(Endpoints.ID) Integer addressId) {
    return addressService.getAddressById(addressId);
  }

  @PatchMapping(Endpoints.URI_ID)
  public Response updateAddress(@PathVariable(Endpoints.ID) Integer addressId,
                                @RequestBody NewAddress address) {
    return addressService.updateAddress(addressId, address);
  }

  @DeleteMapping(Endpoints.URI_ID)
  public Response deleteAddress(@PathVariable(Endpoints.ID) Integer addressId) {
    return addressService.deleteAddress(addressId);
  }

}
